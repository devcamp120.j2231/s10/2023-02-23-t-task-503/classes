import { Retangle } from "./retangle.js";
import { Square } from "./square.js"; 

 var retangle1 = new Retangle(10, 5);
 var retangle2 = new Retangle(20, 5);

 console.log("Retange 1 info:")
 console.log(retangle1._width);
 console.log(retangle1._height);
 console.log(retangle1.getArea());

 console.log("Retange 2 info:")
 console.log(retangle2._width);
 console.log(retangle2._height);
 console.log(retangle2.getArea());

 var square1 = new Square(10, "Đây là hình vuông");
 var square2 = new Square(5);

 console.log("Square 1 info:")
 console.log(square1._width);
 console.log(square1._height);
 console.log(square1._description);
 console.log(square1.getArea());
 console.log(square1.getPerimeter());

 console.log("Square 2 info:")
 console.log(square2._width);
 console.log(square2._height);
 console.log(square2._description);
 console.log(square2.getArea());
 console.log(square2.getPerimeter());

 console.log(retangle1 instanceof Retangle); // T
 console.log(retangle1 instanceof Square); // F
 console.log(square1 instanceof Retangle); // T
 console.log(square1 instanceof Square); // T

 // Hoisting
 a = 1;
 console.log(a);
 var a;