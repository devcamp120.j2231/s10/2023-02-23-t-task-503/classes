import { Retangle } from "./retangle.js";

class Square extends Retangle {
    _description;

    constructor(paramLength, paramDescription = "Mô tả mặc định") {
        super(paramLength, paramLength);

        this._description = paramDescription;
    }

    getPerimeter() {
        return 4 * this._width;
    }
}

export { Square }